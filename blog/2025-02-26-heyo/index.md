---
slug: heyo
title: Heyo!
authors: rwpannick
tags: [hello]
---

Congratulations, you have made your first post!

Feel free to play around and edit this post as much as you like.


```ruby title="test.rb"
#!/usr/bin/env ruby

puts "heyo"

```
